---
title: Academic publications
layout: docs
permalink: /docs/publications/
---

* *Trusting Trust - Reflections on Trusting Trust* (1984) — Ken Thompson. ([PDF](https://www.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf))

* *Fully Countering Trusting Trust through Diverse Double-Compiling* (2005/2009) — David A. Wheeler ([PDF](https://dwheeler.com/trusting-trust/dissertation/wheeler-trusting-trust-ddc.pdf), [...](https://dwheeler.com/trusting-trust/))

* *Functional Package Management with Guix* (2013) — Ludovic Courtès. [[...](https://hal.inria.fr/hal-00824004/en])]

* *Reproducible and User-Controlled Software Environments in HPC with Guix* (2015) — Ludovic Courtès, Ricardo Wurmus [[...](https://hal.inria.fr/hal-01161771/en)]

* *in-toto: Providing farm-to-table guarantees for bits and bytes* (2019) — Santiago Torres-Arias, New York University; Hammad Afzali, New Jersey Institute of Technology; Trishank Karthik Kuppusamy, Datadog; Reza Curtmola, New Jersey Institute of Technology; Justin Cappos, New York University. ([PDF](https://www.usenix.org/system/files/sec19-torres-arias.pdf))


